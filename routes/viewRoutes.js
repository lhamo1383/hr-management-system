const express = require('express')
const router = express.Router()
const viewController = require('../controllers/viewController')
const authController = require('../controllers/authController')

router.get('/dashboard',viewController.getHome)
router.get('/', viewController.getLoginForm)
router.get('/signup', viewController.getSignupForm)
// router.get('/me', authController.protect, viewController.getProfile)
module.exports = router