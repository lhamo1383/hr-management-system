import { showAlert } from "./alert.js"

export const signup = async(name, employeeID, gender, dob, designation, department, appointment_date)=>{
    try{
        const res = await axios({
            method:'POST',
            url:'http://localhost:4001/api/v1/users/signup',
            data:{
                name,
                employeeID,
                gender,
                dob,
                designation,
                department,
                appointment_date,
            },
        })
        if(res.data.status === 'success'){
            showAlert('success','Account created successful!')
            window.setTimeout(()=>{
                location.assign('/')
            },1500)
        }
    }catch(err){
        let message = 
        typeof err.response!=='undefined'
            ? err.response.data.message
            : err.message
        showAlert('error','Error: Passwords are not the same', message)
    }
}
document.querySelector('.form').addEventListener('submit',(e)=>{
    e.preventDefault()
    const name = document.getElementById('name').value
    const employeeID = document.getElementById('employeeID').value
    const gender = document.getElementById('gender').value
    const dob = document.getElementById('dob').value
    const designation = document.getElementById('designation').value
    const department = document.getElementById('department').value
    const appointment = document.getElementById('appointment').value
    console.log("Hellllllooooo"+name,employeeID,gender,dob,designation,department,appointment)
    signup(name,employeeID,gender,dob,designation,department,appointment)
})