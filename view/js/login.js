/* eslint-disable */
// import axios from 'axios';
import { showAlert } from "./alert.js"

const login = async (employeID, password) => {
    try {
      const res = await axios({
        method: 'POST',
        url: 'http://localhost:4001/api/v1/users/login',
        data: {
          employeID,
          password
        }
      });
      console.log(res)
      if (res.data.status === 'success') {
        showAlert('success','Logged in successfully!');
        window.setTimeout(() => {
          location.assign('/');
        }, 1500)
        var obj = res.data.data.user
        console.log(obj)
        document.cookie = ' token = ' + JSON.stringify(obj)
        console.log(obj)
      }
    } catch (err) {
      //console.log(err);
      let message = 
        typeof err.response !== "undefined"
        ? err.response.data.message
        : err.message
      showAlert('error','Error: Incorrect Employe id or password',message)
    }
  };
  
  document.querySelector('.form').addEventListener("submit", (e) => {
    e.preventDefault();
    const employeID = document.getElementById('employeid').value;
    const password = document.getElementById('password').value;
    console.log("Hellow"+ employeID,password)
    login(employeID, password)
  })
  // export const logout = async () => {
  //   try {
  //     const res = await axios({
  //       method: 'GET',
  //       url: 'http://127.0.0.1:3000/api/v1/users/logout'
  //     });
  //     if ((res.data.status = 'success')) location.reload(true);
  //   } catch (err) {
  //     console.log(err.response);
  //     showAlert('error', 'Error logging out! Try again.');
  //   }
  // };