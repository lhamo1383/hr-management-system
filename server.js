const express = require("express")
const path = require('path')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
dotenv.config({path:'./config.env'})
const app = express()

const DB = process.env.DATABASE.replace(
    'PASSWORD',
    process.env.DATABASE_PASSWORD,
)
const local_DB = process.env.DATABASE_LOCAL
//console.log(process.env.DATABASE_PASSWORD)
mongoose.connect(DB).then((con) => {
    //console.log(con.connections)
    console.log('DB connection successful')
}).catch(error => console.log(error));

const userRouter = require('./routes/userRoutes')
const viewRouter = require('./routes/viewRoutes')

const cookieParser = require('cookie-parser')


app.use(express.json())
app.use(cookieParser())
app.use('/api/v1/users', userRouter)
app.use('/',viewRouter)

app.use(express.static(path.join(__dirname, 'view')))


const port = 4001

app.listen(port, () => {
    console.log(`App is running on port ${port} ..`)
})