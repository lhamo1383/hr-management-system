const mongoose = require('mongoose')

const userSchema = new mongoose. Schema({
    name: {type: String, 
    required:[ true,'Please tell us your name!'],
    },
    employeID: {
        type: String,
        required: [true, 'Please provide your employeid'],
        unique: true, 
  
    },
    gender: {
        type: String,
        Enume:['female','male']
    },
    dob: {
        type: String, 

    }, 
    designation: {
        type: String,
    },
    department: {
        type: String,
    },
    password:{
        type:'string'
    }
    
})
const User = mongoose.model ('User', userSchema)
module.exports = User