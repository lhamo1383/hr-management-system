const path = require('path')

exports.getLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname,'../', 'view', 'login.html'))
}
exports.getSignupForm = (req, res) => {
    res.sendFile(path.join(__dirname,'../', 'view', 'signup.html'))
}
exports.getHome = (req, res) => {
    res.sendFile(path.join(__dirname,'../', 'view', 'dashboard.html'))
}

