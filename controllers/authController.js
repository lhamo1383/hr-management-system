const User = require('./../model/userModel')
const { JsonWebTokenError } = require('jsonwebtoken')
const jwt = require('jsonwebtoken')

const signToken = (id) => {
    return jwt.sign({ id }, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_IN,
    })
}
exports.signup = async (req,res,next) => {
    try{
        const newUser =await User.create(req.body)
        const token = signToken(newUser._id)
        res.status(201).json({
            status: "success",
            token,
            data : {
                user: newUser
            }
        })
    }
    catch(err) {
        res.status(500).json({ error: err.message});
    }   
}
exports.login = async (req, res, next) => {
    try {
        console.log(req.headers)
        const { employeID, password } = req.body

        if(!employeID || !password) {
            return next(new AppError('Please provide an email and password!', 400))
        }
        const user = await User.findOne({ employeID }).select('+password')
        //const correct = await user.correctPassword(password, user.password)

        if (!user || !password) {
            return next(new AppError('Incorrect email or password',401))
        }
        // createSendToken(user, 200, res)
        const token = signToken(user._id)
        res.status(200).json({
            status: 'success',
            token,
        })

    } catch (err) {
        res.status(500).json({error: err.message });

    }
}

